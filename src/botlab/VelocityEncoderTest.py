#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import lcm
from lcmtypes import maebot_diff_drive_t

lc = lcm.LCM()

def motorCmdPublish(leftV, rightV):
    cmd = maebot_diff_drive_t()
    cmd.motor_right_speed = rightV 
    cmd.motor_left_speed = leftV
    lc.publish("MAEBOT_DIFF_DRIVE", cmd.encode())

def main():
    speed = float(sys.argv[1])
    motorCmdPublish(speed, speed)

if __name__ == "__main__":
    main()
