import sys, os, time, StringIO
import lcm
import math

import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.backends.backend_agg as agg
import numpy as np

import pygame
from pygame.locals import *

from slam import Slam
from maps import DataMatrix
from laser import RPLidar
from prm import PRMPlanner
from guidance import Guidance

from lcmtypes import maebot_diff_drive_t
from lcmtypes import odo_dxdtheta_t
from lcmtypes import odo_pose_xyt_t
from lcmtypes import pid_init_t
from lcmtypes import velocity_cmd_t
from lcmtypes import rplidar_laser_t
# NEEDS TO ADD OTHER USED LCM TYPES!!!
# SLAM preferences
# CHANGE TO OPTIMIZE
USE_ODOMETRY = True
MAP_QUALITY = 3

# Laser constants
# CHANGE TO OPTIMIZE IF NECSSARY
DIST_MIN = 100; # minimum distance
DIST_MAX = 6000; # maximum distance

# Map constants
# CHANGE TO OPTIMIZE
MAP_SIZE_M = 6.0 # size of region to be mapped [m]
INSET_SIZE_M = 1.0 # size of relative map
MAP_RES_PIX_PER_M = 100 # number of pixels of data per meter [pix/m]
MAP_SIZE_PIXELS = int(MAP_SIZE_M*MAP_RES_PIX_PER_M) # number of pixels across the entire map
MAP_DEPTH = 3 # depth of data points on map (levels of certainty)

# KWARGS
# PASS TO MAP AND SLAM FUNCTIONS AS PARAMETER
KWARGS, gvars = {}, globals()
for var in ['MAP_SIZE_M','INSET_SIZE_M','MAP_RES_PIX_PER_M','MAP_DEPTH','USE_ODOMETRY','MAP_QUALITY']:
    KWARGS[var] = gvars[var] # constants required in modules

rad2degree = lambda x: 180.0*x / math.pi
degree2rad = lambda x: math.pi*x / 180.0

class MainClass:
    def __init__(self, endPos, width=1280, height=960, FPS=10):
        # Data from Handler 
        self.currOdoPos = (0, 0, 0) # store current pose x[mm], y[mm], theta[rad]
        self.currOdoVel = (0, 0, 0) # store current pose dxy[mm], dtheta[rad], dt[s]
        self.currSlamPos = (0, 0, 0)
        # Lidar
        self.lidarAngle = np.array([])
        self.lidarRange = np.array([])
        self.lidarHandled = False 
        # Slam
        self.maebot_laser = RPLidar(DIST_MIN, DIST_MAX)
        self.maebot_slam = Slam(self.maebot_laser, **KWARGS)
        self.maebot_map = DataMatrix(**KWARGS) 
        self.endPos = endPos 
        self.prm = PRMPlanner(**KWARGS)
        self.prm.updateEndPos(endPos, False)
        self.isFinishCurrentGoal = True
        self.guide = Guidance() 
        self.slam_init = True
        # Parameters
        self.velocityScale = 0.5
        self.velCmd = (0.0, 0.0, 0.0, 0.0)
        self.pidParam = [0.001,0.0005,0.0001] #kp, ki, kd
        self.pidParamSelected = -1
        self.pidStep = [0.0001, 0.00001, 0.00001]
        self.selectedColor = (255,0,0)
        self.unselectedColor = (0,0,0)
        # Pygame Params
        pygame.init()
        self.width = width
        self.height = height
        self.screen = pygame.display.set_mode((self.width, self.height))

        # LCM Subscribe
        self.lc = lcm.LCM()
        self.lc.subscribe("BOT_ODO_VEL_TEAM16", self.OdoVelocityHandler)
        self.lc.subscribe("BOT_ODO_POSE_TEAM16", self.OdoPoseHandler)
        self.lc.subscribe("RPLIDAR_LASER_TEAM16", self.LidarHandler)
        # Prepare Figure for Lidar
        # figsize:  in inches
        # dpi:      100 dots per inch, so the resulting buffer is 400x400 pixels
        self.fig = plt.figure(figsize=[3, 3], dpi=100)  
        self.fig.patch.set_facecolor('white')
        self.fig.add_axes([0,0,1,1],projection='polar')
        self.surf = None
        self.ax = self.fig.gca()
        self.font = pygame.font.SysFont("DejaVuSans Mono", 14)

        # Prepare Figure for SLAM
        self.slamImg = None
	self.pathPlanningSlam = None

    def plotLidarData(self):
        # Plot Lidar Scans
        # IMPLEMENT ME - CURRENTLY ONLY PLOTS ONE DOT
        if self.lidarHandled and not self.slam_init:
            plt.cla()
            self.ax.plot(0,0,'or',markersize=2)
            self.ax.plot(self.lidarAngle, self.lidarRange, 'bo', markersize=2)
            self.ax.set_rmax(1.5)
            self.ax.set_theta_direction(-1)
            self.ax.set_theta_zero_location("N")
            self.ax.set_thetagrids([0,45,90,135,180,225,270,315],labels=['','','','','','','',''], frac=None,fmt=None)
            self.ax.set_rgrids([0.5,1.0,1.5],labels=['0.5','1.0',''],angle=None,fmt=None)
            self.lidarHandled = False
            canvas = agg.FigureCanvasAgg(self.fig)
            canvas.draw()
            renderer = canvas.get_renderer()
            raw_data = renderer.tostring_rgb()
            size = canvas.get_width_height()
            self.surf = pygame.image.fromstring(raw_data, size, "RGB")
            mapImg = self.maebot_map.returnImage()
            mode = mapImg.mode
            size = mapImg.size
            data = mapImg.tostring()
            self.slamImg = pygame.image.fromstring(data, size, mode)
	    self.pathPlanningSlam = pygame.surfarray.make_surface(self.prm.breezyMapReducedExpandObstacle.astype('uint8'))
            self.screen.blit(self.surf, (320,0))     
            self.screen.blit(self.slamImg, (640,0))
	    self.screen.blit(self.pathPlanningSlam, (640, 620))
        else:
            if self.surf is not None:
                self.screen.blit(self.surf, (320,0))
            if self.slamImg is not None:
                self.screen.blit(self.slamImg, (640,0))
	    if self.pathPlanningSlam is not None:
		self.screen.blit(self.pathPlanningSlam, (640, 620))

    def displayOdoPoseVelData(self):
        # Position and Velocity Feedback Text on Screen
        pygame.draw.rect(self.screen,(0,0,0),(5,350,350,120),2)
        text = self.font.render("  POSITION  ",True,(0,0,0))
        self.screen.blit(text,(10,360))
        text = self.font.render("x: %2f [mm]" % self.currOdoPos[0], True,(0,0,0))
        self.screen.blit(text,(10,390))
        text = self.font.render("y: %2f [mm]" % self.currOdoPos[1], True,(0,0,0))
        self.screen.blit(text,(10,420))
        text = self.font.render("t: %2f [deg]" % self.currOdoPos[2], True,(0,0,0))
        self.screen.blit(text,(10,450))
 
	if self.currOdoVel[2] == 0:
	    text = self.font.render("  VELOCITY  ",True,(0,0,0))
            self.screen.blit(text,(150,360))
       	    text = self.font.render("dxy/dt: N/A [mm/s]",True,(0,0,0))
            self.screen.blit(text,(150,390))
            text = self.font.render("dth/dt: N/A [deg/s]",True,(0,0,0))
            self.screen.blit(text,(150,420))
            text = self.font.render("dt:  N/A  [s]",True,(0,0,0))
            self.screen.blit(text,(150,450))
	else:
            text = self.font.render("  VELOCITY  ",True,(0,0,0))
            self.screen.blit(text,(150,360))
            text = self.font.render("dxy/dt: %2f [mm/s]" % (self.currOdoVel[0]/self.currOdoVel[2]),True,(0,0,0))
            self.screen.blit(text,(150,390))
            text = self.font.render("dth/dt: %2f [deg/s]" % (self.currOdoVel[1]/self.currOdoVel[2]),True,(0,0,0))
            self.screen.blit(text,(150,420))
            text = self.font.render("dt:   %2f [s]" % self.currOdoVel[2],True,(0,0,0))
            self.screen.blit(text,(150,450))
    
    def displayPIDParams(self):
        pygame.draw.rect(self.screen, (0,0,0), (5, 10, 300, 330), 2)
        widthPixelStart = 20
        heightPixelStart = 30
        for i in xrange(3):
            if i == self.pidParamSelected:
                pygame.draw.rect(self.screen, self.selectedColor, (widthPixelStart+i*95, heightPixelStart, 80, 80),2)
            else:
                pygame.draw.rect(self.screen, self.unselectedColor, (widthPixelStart+i*95, heightPixelStart, 80, 80),2)
		
	PIDfont = pygame.font.SysFont("DejaVuSans Mono", 24)
        text = PIDfont.render("P", True, (0,0,0))
        self.screen.blit(text, (55, 58))
	text = PIDfont.render("I", True, (0,0,0))
        self.screen.blit(text, (150, 58))
	text = PIDfont.render("D", True, (0,0,0))
        self.screen.blit(text, (245, 58))
        heightPixelStart = 150
        for i in xrange(3):
            text = self.font.render("%f" %self.pidParam[i], True, (0,0,0))
            self.screen.blit(text, (widthPixelStart+95*i, heightPixelStart))
        text = self.font.render("Press, P, I, D to select parameters", True, (0,0,0))
        self.screen.blit(text, (15, 200))
	text = self.font.render("Press + and - to change value", True, (0,0,0))
        self.screen.blit(text, (15, 220))
	text = self.font.render("Press c to clear value", True, (0,0,0))
        self.screen.blit(text, (15, 240))
	text = self.font.render("Press z to unselect parameters", True, (0,0,0))
        self.screen.blit(text, (15, 260))

    def OdoVelocityHandler(self, channel, data): 
        msg = odo_dxdtheta_t.decode(data)
        self.currOdoVel = (msg.dxy, msg.dtheta, msg.dt)

    def OdoPoseHandler(self, channel, data):
        msg = odo_pose_xyt_t.decode(data)
        self.currOdoPos = (msg.xyt[0], msg.xyt[1], msg.xyt[2])
 
    def LidarHandler(self, channel, data): 
        msg = rplidar_laser_t.decode(data)
        self.lidarRange = np.array(msg.ranges)
        self.lidarAngle = np.array(msg.thetas)
        self.lidarHandled = True
        lidar = [(0, 0) for i in xrange(len(msg.thetas))]
        for i in xrange(len(msg.thetas)):
            lidar[i] = (msg.ranges[i], rad2degree(msg.thetas[i]))
        self.currSlamPos = self.maebot_slam.updateSlam(lidar, self.currOdoVel)
        if self.slam_init:
            #self.prm.readMapFromFile("test_map.npy")
            self.maebot_map.getRobotPos(self.currSlamPos, init=self.slam_init)
            self.slam_init = False
        else:
            self.maebot_map.getRobotPos(self.currSlamPos)
        #print self.currSlamPos
        #self.maebot_map.getRobotPos(self.currSlamPos)
        self.maebot_map.drawBreezyMap(self.maebot_slam.getBreezyMap())
        if self.isFinishCurrentGoal:
	    print "recalc Path"
            path = self.prm.solvePath(self.currSlamPos, self.maebot_map.getMapMatrix(), False)
            #self.maebot_map.saveImage()
            if len(path) > 0:
                print path
                self.guide.plan = []
                step_num_calculation=1
                self.guide.plan.append([path[0][0], path[0][1]])
                self.guide.nleg=step_num_calculation
                print self.guide.plan
                self.guide.start()
                self.isFinishCurrentGoal = False

    def motorCmdPublish(self, leftV, rightV):
        cmd = maebot_diff_drive_t()
        cmd.motor_right_speed = self.velocityScale * rightV
        cmd.motor_left_speed = self.velocityScale * leftV
        self.lc.publish("MAEBOT_DIFF_DRIVE_TEAM16", cmd.encode())

    def velocityCmdPublish(self):
        cmd = velocity_cmd_t()
        cmd.utime = time.time()*1000000
        cmd.FwdSpeed = self.velCmd[0]
        cmd.AngSpeed = self.velCmd[1]
        cmd.Distance = self.velCmd[2]
        cmd.Angle = self.velCmd[3]
        # print "publish vel cmd", self.velCmd 
        self.velCmd = (0.0, 0.0, 0.0, 0.0)
	self.lc.publish("GS_VELOCITY_CMD_TEAM16", cmd.encode())

    def pidInitPublish(self):
	msg = pid_init_t()
	msg.utime = time.time()*1000000
	msg.kp = self.pidParam[0]
	msg.ki = self.pidParam[1]
	msg.kd = self.pidParam[2]
	self.lc.publish("GS_PID_INIT_TEAM16", msg.encode())

    def MainLoop(self):
        pygame.key.set_repeat(1, 20)
        vScale = 0.5
	runflag=0
        # Prepare Text to Be output on Screen
        while 1:
            self.lc.handle()      
            #use currSlamPos or currOdoPos should be fine
            if not self.isFinishCurrentGoal:
                self.guide.guide_and_command(self.currSlamPos)
                if self.guide.state == -1:
                    self.prm.path.pop()
                    self.isFinishCurrentGoal = True
            if self.prm.convertToReduced(self.currSlamPos) == self.prm.convertToReduced(self.endPos):
                break
	    self.pidInitPublish()
            self.screen.fill((255,255,255))
            self.plotLidarData()
            self.displayOdoPoseVelData()    
            self.displayPIDParams()
            pygame.display.flip()

if __name__ == "__main__":
    MainWindow = MainClass((3000, 500))
    MainWindow.MainLoop()
