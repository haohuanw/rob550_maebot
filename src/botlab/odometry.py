# odometry.py
#
# skeleton code for University of Michigan ROB550 Botlab
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys, os, time
import lcm
from math import *

from breezyslam.robots import WheeledRobot

from lcmtypes import maebot_motor_feedback_t
from lcmtypes import maebot_sensor_data_t
from lcmtypes import odo_pose_xyt_t
from lcmtypes import odo_dxdtheta_t

class Maebot(WheeledRobot):

    def __init__(self):
        self.wheelDiameterMillimeters = 32.0     # diameter of wheels [mm]
        self.axleLengthMillimeters = 80.0        # separation of wheels [mm]  
        self.ticksPerRev = 16.0                  # encoder tickers per motor revolution
        self.gearRatio = 30.0                    # 30:1 gear ratio
        self.enc2mm = (pi * self.wheelDiameterMillimeters) / (self.gearRatio * self.ticksPerRev) # encoder ticks to distance [mm]
        self.degree2rad = pi/180.0
        self.LSB = 131.0
        self.delta_theta_thres = 0.001 

        self.prevEncPos = (0,0,0)           # store previous readings for odometry
        self.prevOdoPos = (0,0,0)           # store previous x [mm], y [mm], theta [rad]
        self.currEncPos = (0,0,0)           # current reading for odometry
        self.currOdoPos = (0,0,0)           # store odometry x [mm], y [mm], theta [rad]
        self.currOdoVel = (0,0,0)           # store current velocity dxy [mm], dtheta [rad], dt [s]
        self.motor_state = 0
        self.sensor_state = 0
        self.gyro_z_prev = 0.0
        self.gyro_z_curr = 0.0
        WheeledRobot.__init__(self, self.wheelDiameterMillimeters/2.0, self.axleLengthMillimeters/2.0)

        # LCM Initialization and Subscription
        self.lc = lcm.LCM()
        lcmMotorSub = self.lc.subscribe("MAEBOT_MOTOR_FEEDBACK_TEAM16", self.motorFeedbackHandler)
        lcmSensorSub = self.lc.subscribe("MAEBOT_SENSOR_DATA_TEAM16", self.sensorDataHandler)

    def calcVelocities(self):
        # IMPLEMENT ME
        # TASK: CALCULATE VELOCITIES FOR ODOMETRY
        # Update self.currOdoVel and self.prevOdoVel
        left_diff = self.currEncPos[0] - self.prevEncPos[0]
        right_diff = self.currEncPos[1] - self.prevEncPos[1]
        dt = (self.currEncPos[2] - self.prevEncPos[2])*0.000001
        dxy = (left_diff + right_diff)*self.enc2mm / 2.0
        dtheta = (left_diff - right_diff)*self.enc2mm / self.axleLengthMillimeters
	#print dxy, dt , dtheta
        self.prevOdoVel = self.currOdoVel 
        self.currOdoVel = (dxy, dtheta, dt)

    def getVelocities(self):
        # IMPLEMENT ME
        # TASK: RETURNS VELOCITY TUPLE
        # Return a tuple of (dxy [mm], dtheta [rad], dt [s])
        dxy = self.currOdoVel[0]
        dtheta = self.currOdoVel[1]
        dt = self.currOdoVel[2]
        return (dxy, dtheta, dt) # [mm], [rad], [s]

    def calcOdoPosition(self):
        # IMPLEMENT ME
        # TASK: CALCULATE POSITIONS
        # Update self.currOdoPos and self.prevOdoPos
        #print self.prevOdoPos
        prev_x = self.prevOdoPos[0]
        prev_y = self.prevOdoPos[1]
        prev_theta = self.prevOdoPos[2]
        dxy = self.currOdoVel[0]
        dtheta = self.currOdoVel[1]
        dt = self.currOdoVel[2]
        x = prev_x + dxy*cos(prev_theta)
        y = prev_y + dxy*sin(prev_theta)
        theta= prev_theta + dtheta
        #print dxy, prev_theta, dt
        #print x, y, theta
        self.currOdoPos = (x, y, theta)
        self.prevOdoPos = self.currOdoPos

    def getOdoPosition(self):
        # IMPLEMENT ME
        # TASK: RETURNS POSITION TUPLE
        # Return a tuple of (x [mm], y [mm], theta [rad])
        x = self.currOdoPos[0];
        y = self.currOdoPos[1];
        theta = self.currOdoPos[2];
        return (x, y, theta) # [mm], [rad], [s]

    def publishOdometry(self):
        msg = odo_pose_xyt_t()
        msg.utime = time.time()*1000000
        msg.xyt[0] = self.currOdoPos[0]
        msg.xyt[1] = self.currOdoPos[1]  
        msg.xyt[2] = self.currOdoPos[2]  
        self.lc.publish("BOT_ODO_POSE_TEAM16", msg.encode())
        # IMPLEMENT ME
        # TASK: PUBLISHES BOT_ODO_POSE MESSAGE

    def publishVelocities(self):
        msg = odo_dxdtheta_t()
        msg.utime = time.time()*1000000
        msg.dxy = self.currOdoVel[0]
        msg.dtheta = self.currOdoVel[1]
        msg.dt = self.currOdoVel[2]
        self.lc.publish("BOT_ODO_VEL_TEAM16", msg.encode())
        # IMPLEMENT ME
        # TASK: PUBLISHES BOT_ODO_VEL MESSAGE

    def motorFeedbackHandler(self,channel,data):
        msg = maebot_motor_feedback_t.decode(data)
        if self.motor_state == 0:
            self.prevEncPos = (msg.encoder_left_ticks, msg.encoder_right_ticks, msg.utime)
            self.motor_state = 1
        elif self.motor_state == 1:
            self.currEncPos = (msg.encoder_left_ticks, msg.encoder_right_ticks, msg.utime)
            self.motor_state = 2
        else:
            self.currEncPos = (msg.encoder_left_ticks, msg.encoder_right_ticks, msg.utime)
            self.calcVelocities()
            self.calcOdoPosition()
            self.prevEncPos = self.currEncPos
            # TASK: PROCESS ENCODER DATA
            # get encoder positions and store them in robot,
            # update robots position and velocity estimate

    def sensorDataHandler(self,channel,data):
        msg = maebot_sensor_data_t.decode(data)
        if self.sensor_state == 0:
            self.gyro_z_pre = msg.gyro_int[2]
            self.sensor_state = 1
        elif self.sensor_state == 1:
            self.gyro_z_curr = msg.gyro_int[2]
            self.sensor_state = 2
        else:
            self.gyro_z_curr = msg.gyro_int[2]
            gyro_dtheta= (self.gyro_z_curr - self.gyro_z_prev)/self.LSB * self.degree2rad * 0.000001
            diff = abs(self.currOdoVel[1] - gyro_dtheta)
	    self.gyro_z_prev = self.gyro_z_curr
            if diff > self.delta_theta_thres:
	    	dxy = self.currOdoVel[0]
	    	dt = self.currOdoVel[2]
                self.currOdoVel =(dxy, gyro_dtheta, dt)
            # IMPLEMENT ME
            # TASK: PROCESS GYRO DATA

    def MainLoop(self):
        oldTime = time.time()
        frequency = 20;
        while(1):
            self.lc.handle()
            if(time.time()-oldTime > 1.0/frequency):
                self.publishOdometry()
                self.publishVelocities()
                oldTime = time.time()   

if __name__ == "__main__":
    robot = Maebot()
    robot.MainLoop()  
