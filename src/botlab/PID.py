# PID.py
#
# skeleton code for University of Michigan ROB550 Botlab
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys, os, time
import lcm, signal
import math

from lcmtypes import maebot_diff_drive_t
from lcmtypes import maebot_motor_feedback_t
from lcmtypes import pid_init_t
from lcmtypes import velocity_cmd_t
from lcmtypes import pid_debug_t
from lcmtypes import odo_pose_xyt_t
from lcmtypes import maebot_laser_t

###############################################
# PID CLASS - SIMILAR TO C CODE FOR CTRLPANEL
###############################################
class TicksPID():
    def __init__(self, kp, ki, kd, trim):
        # Main Param
        self.kp = kp
        self.ki = ki
        self.kd = kd
        self.trim = trim 
        # Integral
        self.iTerm = 0.0
        self.iTermMin = -1.0
        self.iTermMax = 1.0
        # Input
        self.input = 0.0
        self.dotinput = 0.0
        # Output
        self.output = 0.0
        self.outputMin = -0.6
        self.outputMax = 0.6
        # Reference 
        self.dotsetpoint = 0.0 # ticks per second 
        self.setpoint = 0.0 # ticks
        self.updateRate = 0.1
        # Compute
        self.error = 0.0
        self.errordot = 0.0

    def Compute(self):
        #self.output += self.kp*(self.input-self.prevInput)\
                #	      +self.ki*self.input\
                #	      +self.kd*(self.input-2*self.prevInput+self.preprevInput)
        #print "Pos", currErrorPos, "Vel", currErrorVel
        self.setpoint = self.setpoint + self.dotsetpoint 
        self.error = self.setpoint - self.input #ticks
        self.errordot = self.dotsetpoint - self.dotinput #ticks / update
        # print self.kp, self.ki, self.kd
        if abs(self.error) < 0.1:
            self.iTerm = 0
        else:
            self.iTerm += self.error 

        if self.iTerm > self.iTermMax:
            self.iTerm = self.iTermMax
        if self.iTerm < self.iTermMin:
            self.iTerm = self.iTermMin

        self.output = (self.kp * self.error + self.ki * self.iTerm +\
                self.kd * self.errordot)*1 + self.trim

        if self.output > self.outputMax:
            self.output = self.outputMax
        if self.output < self.outputMin:
            self.output = self.outputMin
        return self.output

    # Accessory Function to Change Gains
    # Update if you are not using updateRate in Compute()
    def SetTunings(self, kp, ki, kd):
        self.kp = kp
        self.ki = ki * self.updateRate 
        self.kd = kd / self.updateRate 

    def SetIntegralLimits(self, imin, imax):
        self.iTermMin = imin
        self.iTermMax = imax

    def SetOutputLimits(self, outmin, outmax):
        self.outputMin = outmin
        self.outputMax = outmax

    def SetUpdateRate(self, rateInSec):
        self.updateRate = rateInSec


class PIDSpeedController():
    def __init__(self):
        # Create Both PID for controller
        self.leftTicksController = TicksPID(0.001, 0.0005, 0.0001, 0.2) #Kp Ki Kd Trim
        self.rightTicksController = TicksPID(0.001, 0.0005, 0.0001, 0.2) #Kp Ki Kd Trim

        # CMD Msg
        self.currVelCmd = (0, 0)
        self.targetTicks = (0, 0)
	self.targetRad = 0.0

        # Motor Feedback Msg
        self.prevTicksCounts = (0.0, 0.0, 0.0) # Left, Right, Utime
        self.currTicksCounts = (0.0, 0.0, 0.0) # Left, Right, Utime
        self.updateTicksCounts = (0.0, 0.0) # Left, Right
        self.motorFeedbackState = 0

        # Odo Msg
        self.xyt = (0.0, 0.0, 0.0)

        # PID Params
        self.kp = 0.0
        self.ki = 0.0
        self.kd = 0.0

        # LCM Subscribe
        self.lc = lcm.LCM()
        #self.lc.subscribe("GS_PID_INIT", self.pidParamHandler)
        self.lc.subscribe("GS_VELOCITY_CMD_TEAM16", self.velocityCMDHandler)
        self.lc.subscribe("MAEBOT_MOTOR_FEEDBACK_TEAM16", self.motorFeedbackHandler)
        self.lc.subscribe("BOT_ODO_POSE_TEAM16", self.botPoseHandler)
        signal.signal(signal.SIGINT, self.signal_handler)

        # Output
        self.leftSpeed = 0.0
        self.rightSpeed = 0.0

        # Solution to check that lcm handle will reveive two msg (pos and vel)
        # before executing further code
        self.msg_counter = [0, 0]

        # odometry
        self.wheelDiameterMillimeters = 32.0  # diameter of wheels [mm]
        self.axleLengthMillimeters = 80.0     # separation of wheels [mm]    
        self.ticksPerRev = 16.0           # encoder tickers per motor revolution
        self.gearRatio = 30.0             # 30:1 gear ratio
        self.enc2mm = ((math.pi * self.wheelDiameterMillimeters) / (self.gearRatio * self.ticksPerRev))

    def publishMotorCmd(self):
        cmd = maebot_diff_drive_t()
        cmd.motor_left_speed = self.leftSpeed
        cmd.motor_right_speed = self.rightSpeed
        #print self.rightSpeed, self.leftSpeed
        self.lc.publish("MAEBOT_DIFF_DRIVE_TEAM16", cmd.encode())

    def pidParamHandler(self,channel,data):
        msg = pid_init_t.decode(data)
        self.kp = msg.kp
        self.ki = msg.ki
        self.kd = msg.kd
    
    def botPoseHandler(self,channel,data):
        msg = odo_pose_xyt_t.decode(data)
        self.xyt = (msg.xyt[0], msg.xyt[1], msg.xyt[2])

    def velocityCMDHandler(self,channel,data):
        pass
        # msg = velocity_cmd_t.decode(data)
        # self.currVelCmd = (msg.leftWheelCmd, msg.rightWheelCmd)
        # self.msg_counter[0] += 1

    def motorFeedbackHandler(self,channel,data):
        msg = maebot_motor_feedback_t.decode(data)
        if self.motorFeedbackState == 0:
            self.prevTicksCounts = (msg.encoder_left_ticks, msg.encoder_right_ticks, msg.utime)
            self.motorFeedbackState = 1
        elif self.motorFeedbackState == 1:
            self.currTicksCounts = (msg.encoder_left_ticks, msg.encoder_right_ticks, msg.utime)
            self.leftTicksController.setpoint = msg.encoder_left_ticks
            self.rightTicksController.setpoint =  msg.encoder_right_ticks
            self.motorFeedbackState = 2
        else:
            self.prevTicksCounts = self.currTicksCounts
            self.currTicksCounts = (msg.encoder_left_ticks, msg.encoder_right_ticks, msg.utime)

            leftDist = self.currTicksCounts[0] - self.prevTicksCounts[0]
            rightDist = self.currTicksCounts[1] - self.prevTicksCounts[1]
            self.updateTicksCounts = (leftDist, rightDist)
            rateInSec = (self.currTicksCounts[2] - self.prevTicksCounts[2])*0.000001

            self.leftTicksController.SetUpdateRate(rateInSec)
            self.rightTicksController.SetUpdateRate(rateInSec) 
            self.leftTicksController.dotinput = self.updateTicksCounts[0]
            self.rightTicksController.dotinput = self.updateTicksCounts[1]
            self.leftTicksController.input = self.currTicksCounts[0]
            self.rightTicksController.input = self.currTicksCounts[1]
            self.msg_counter[1] += 1

    def turnRad(self, rad):
	self.motorFeedbackState = 0
	while(1):
            self.lc.handle()
               #if self.msg_counter[0] * self.msg_counter[1] > 0: 
            if self.motorFeedbackState == 2:
                break
	self.leftTicksController.trim = 0.0
	self.rightTicksController.trim = 0.0
        self.targetRad = self.xyt[2] + rad
        #diff = abs(rad*self.axleLengthMillimeters / self.enc2mm)
        if rad >= 0:
            #self.targetTicks = (self.leftTicksController.setpoint + diff/2.0, self.rightTicksController.setpoint - diff/2.0)
            self.leftTicksController.dotsetpoint = 6.0
            self.rightTicksController.dotsetpoint = -6.0
        else:
            #self.targetTicks = (self.leftTicksController.setpoint - diff/2.0, self.rightTicksController.setpoint + diff/2.0)
            self.leftTicksController.dotsetpoint = -6.0
            self.rightTicksController.dotsetpoint = 6.0
        while 1:
            self.lc.handle()
            self.leftSpeed = self.leftTicksController.Compute()
            self.rightSpeed = self.rightTicksController.Compute()
	    #print self.targetTicks
	    #print self.leftTicksController.input, self.rightTicksController.input
            #if abs(self.leftTicksController.input - self.targetTicks[0]) <=15:
	    print self.xyt[2], self.targetRad
	    if abs(self.xyt[2] - self.targetRad) < 0.02:		
		self.stop_maebot()
                break
	    if rad>0 and self.xyt[2] >= self.targetRad:
		self.stop_maebot()
		break
	    if rad<0 and self.xyt[2] <= self.targetRad:
	        self.stop_maebot()
		break
            #if abs(self.rightTicksController.input - self.targetTicks[1]) <=15:
		#self.stop_maebot()
                #break
            self.publishPIDDebugInfo()
            self.msg_counter[0] -= 1
            self.msg_counter[1] -= 1
            self.publishMotorCmd() 

    # move to target ticks
    def moveStraight(self, distInmm):
	self.motorFeedbackState = 0
	while(1):
            self.lc.handle()
               #if self.msg_counter[0] * self.msg_counter[1] > 0: 
            if self.motorFeedbackState == 2:
                break
	self.leftTicksController.trim = 0.2
	self.rightTicksController.trim = 0.2
        self.leftTicksController.dotsetpoint = 15.0
        self.rightTicksController.dotsetpoint = 15.0
        self.targetTicks = (self.leftTicksController.setpoint+ distInmm / self.enc2mm, self.rightTicksController.setpoint + distInmm/self.enc2mm)
        while 1:
            self.lc.handle()
            self.leftSpeed = self.leftTicksController.Compute()
            self.rightSpeed = self.rightTicksController.Compute()
            if abs(self.leftTicksController.input - self.targetTicks[0]) <=15 :
		self.stop_maebot()
                break
            if abs(self.rightTicksController.setpoint - self.targetTicks[1]) <=15 :
		self.stop_maebot()
                break
            self.publishPIDDebugInfo()
            self.msg_counter[0] -= 1
            self.msg_counter[1] -= 1
            self.publishMotorCmd() 

    # Main Program Loop
    def Controller(self):
        # For now give a fixed command here
        # Later code to get from groundstation should be used
	for i in xrange(6):
	    #self.moveStraight(1000)
	    #time.sleep(1)
	    self.turnRad(-math.pi/3.0)
	    time.sleep(1)
	    #self.motorFeedbackState = 0
	    #while(1):
            #    self.lc.handle()
               #if self.msg_counter[0] * self.msg_counter[1] > 0: 
            #    if self.motorFeedbackState == 2:
            #        break
            #self.moveStraight(1000)

	    #self.motorFeedbackState = 0
	    #while(1):
            #    self.lc.handle()
               #if self.msg_counter[0] * self.msg_counter[1] > 0: 
            #    if self.motorFeedbackState == 2:
            #        break
	    #self.turnRad(math.pi/2.0)
	    #time.sleep(1)


    def publishPIDDebugInfo(self):
        msg = pid_debug_t()
        msg.utime = time.time()*1000000
        msg.left_error = self.leftTicksController.error
        msg.left_error_dot = self.leftTicksController.errordot
        msg.left_input = self.leftTicksController.input
        msg.left_dotinput = self.leftTicksController.dotinput
        msg.left_output = self.leftTicksController.output
        msg.right_error = self.rightTicksController.error
        msg.right_error_dot = self.rightTicksController.errordot
        msg.right_input = self.rightTicksController.input
        msg.right_dotinput = self.rightTicksController.dotinput
        msg.right_output = self.rightTicksController.output
        self.lc.publish("PID_DEBUG_INFO_TEAM16", msg.encode())	 

    # Function to print 0 commands to morot when exiting with Ctrl+C 
    # No need to change 
    def signal_handler(self,  signal, frame):
        print("Terminating!")
        for i in range(5):
            cmd = maebot_diff_drive_t()
            cmd.motor_right_speed = 0.0
            cmd.motor_left_speed = 0.0	
            self.lc.publish("MAEBOT_DIFF_DRIVE_TEAM16", cmd.encode())
        exit(1)

    def stop_maebot(self):
	for i in range(5):
            cmd = maebot_diff_drive_t()
            cmd.motor_right_speed = 0.0
            cmd.motor_left_speed = 0.0	
            self.lc.publish("MAEBOT_DIFF_DRIVE_TEAM16", cmd.encode())



###################################################
# MAIN FUNCTION
###################################################
if __name__ == "__main__":
    pid = PIDSpeedController()
    pid.Controller()

    
