#!/usr/bin/env python
# -*- coding: utf-8 -*-
import copy
import sys
import math
import numpy as np
import matplotlib.pyplot as plt
import pygame
from pygame.locals import *
from scipy import misc 
import cProfile

float2int = lambda x: int(0.5+x)
DEG_2_RAD = np.pi/180.0

def do_cprofile(func):
    def profiled_func(*args, **kwargs):
        profile = cProfile.Profile()
        try:
            profile.enable()
            result = func(*args, **kwargs)
            profile.disable()
            return result
        finally:
            profile.print_stats()
    return profiled_func

class PRMPlanner():

    def __init__(self, breezyMap=None, sampleSize=3000, edgeOffset=2, reduceRatio=5, neighborDist=6, MAP_SIZE_M=6.0, MAP_RES_PIX_PER_M=100, **unused):
        if breezyMap is not None:
            self.updateBreezyMap(breezyMap)
        else:
            self.breezyMap = breezyMap #[y][x], [row][col] for indexing
        self.samples = {} 
        self.sampleSize = sampleSize
        self.edgeOffset = edgeOffset 
        self.reduceRatio = reduceRatio
        self.neighborDist = neighborDist
        self.path = []
        self.overallPath = []
        self.mm2pix = MAP_RES_PIX_PER_M/1000.0 # [pix/mm]
        self.mapSize_pix = int(MAP_SIZE_M*MAP_RES_PIX_PER_M)
        # each time just update the map around maebot
        self.reducedSize = self.mapSize_pix/self.reduceRatio
        self.breezyMapReduced = np.array([[255 for i in xrange(self.reducedSize)] for j in xrange(self.reducedSize)])
        self.breezyMapReducedExpandObstacle = np.array([[255 for i in xrange(self.reducedSize)] for j in xrange(self.reducedSize)])
        self.start = (-1, -1)
        self.startReduced = (-1, -1)

    def updateStartPos(self, startPos, isPix=True):
        if isPix:
            self.lastStart = self.start
            self.lastStartReduced = self.startReduced
            self.start = (startPos[1], startPos[0])
            self.startReduced = (startPos[1]/self.reduceRatio, startPos[0]/self.reduceRatio)
        else:
            self.lastStart = self.start
            self.lastStartReduced = self.startReduced
            xpix = float2int(startPos[0]*self.mm2pix)
            ypix = self.mapSize_pix-float2int(startPos[1]*self.mm2pix)
            self.start = (ypix, xpix)
            self.startReduced = (ypix/self.reduceRatio, xpix/self.reduceRatio)
            #print "update start: ", startPos, "Reduced:", self.startReduced
        #print self.startReduced

    def updateEndPos(self, endPos, isPix=True):
        if isPix:
            self.end = (endPos[1], endPos[0])
            self.endReduced = (endPos[1]/self.reduceRatio, endPos[0]/self.reduceRatio)
            #print self.endReduced
        else:
            xpix = float2int(endPos[0]*self.mm2pix)
            ypix = self.mapSize_pix-float2int(endPos[1]*self.mm2pix)
            self.start = (ypix, xpix)
            self.endReduced = (ypix/self.reduceRatio, xpix/self.reduceRatio)

    def updateBreezyMap(self, breezyMap):
        self.originalBreezyMap = copy.copy(breezyMap)
        # self.breezyMap = breezyMap
        # self.breezyMapReduced = breezyMap[::self.reduceRatio,::self.reduceRatio]
        row_start = max(self.startReduced[0]-10, 0)
        row_end = min(self.startReduced[0]+10, self.reducedSize)
        col_start = max(self.startReduced[1]-10, 0)
        col_end = min(self.startReduced[1]+10, self.reducedSize)
        #self.breezyMapReduced[row_start:row_end+1, col_start:col_end+1] = breezyMap[row_start*self.reduceRatio:row_end*self.reduceRatio+1:self.reduceRatio, col_start*self.reduceRatio:col_end*self.reduceRatio+1:self.reduceRatio]
	self.breezyMapReduced = breezyMap[::self.reduceRatio, ::self.reduceRatio]
        for i in xrange(row_start, row_end):
            for j in xrange(col_start, col_end):
                if self.breezyMapReduced[i][j] < 128:
                    self.breezyMapReduced[i][j] = 0
                else:
                    self.breezyMapReduced[i][j] = 255

        for i in xrange(row_start+self.edgeOffset, row_end-self.edgeOffset+1):
            for j in xrange(col_start+self.edgeOffset, col_end-self.edgeOffset+1):
                if self.breezyMapReduced[i][j] == 0:
                    for m in xrange(self.edgeOffset):
                        for n in xrange(self.edgeOffset): 
                            self.breezyMapReducedExpandObstacle[i+m][j+n] = 0
                            self.breezyMapReducedExpandObstacle[i-m][j-n] = 0

    def randomSample(self):
        row = len(self.breezyMapReducedExpandObstacle)
        col = len(self.breezyMapReducedExpandObstacle[0])
        # assume row == col
        self.samples.clear()
        s = np.random.random_integers(row-1, size=(self.sampleSize, 2)) 
        for i in xrange(len(s)):
            self.samples[(s[i][0], s[i][1])] = []
        # print len(self.samples)

    def reduceSamples(self):
        for key,v in self.samples.items():
            if self.breezyMapReducedExpandObstacle[key[0]][key[1]] == 0:
                self.samples.pop(key)
        self.sampleSize = len(self.samples)

    def connectSamples(self):
        self.samples[self.startReduced] = []
        self.samples[self.endReduced] = []
        for key in self.samples:
            for i in xrange(self.neighborDist):
                for j in xrange(self.neighborDist):
                    dest1 = (key[0]-i, key[1]-j)
                    dest2 = (key[0]+i, key[1]+j)
                    dest3 = (key[0]+i, key[1]-j)
                    dest4 = (key[0]-i, key[1]+j)
                    if dest1 in self.samples:
                        if self.checkCanConnect(key, dest1):
                            self.samples[key].append(dest1)
                    if dest2 in self.samples:
                        if self.checkCanConnect(key, dest2):
                            self.samples[key].append(dest2)
                    if dest3 in self.samples:
                        if self.checkCanConnect(key, dest3):
                            self.samples[key].append(dest3)
                    if dest4 in self.samples:
                        if self.checkCanConnect(key, dest4):
                            self.samples[key].append(dest4)
            #print "key: ", (key[0], key[1]), "neighbors: ", self.samples[key]
    
    def checkCanConnect(self, point1, point2):
        if self.breezyMapReducedExpandObstacle[point1[0]][point1[1]] == 0:
            return False
        if self.breezyMapReducedExpandObstacle[point2[0]][point2[1]] == 0:
            return False
        x1 = point1[1]
        x2 = point2[1]
        y1 = point1[0]
        y2 = point2[0]
        steep = abs(y2-y1) > abs(x2-x1)
        if steep:
            x1, y1 = y1, x1
            x2, y2 = y2, x2
        if x1 > x2:
            x1, x2 = x2, x1
            y1, y2 = y2, y1
        dx = x2 - x1
        dy = abs(y2 - y1)
        f = dx/2
        ystep = 1 if y1<y2 else -1
        y = y1
        for x in xrange(x1, x2+1):
            if steep:
                if self.breezyMapReducedExpandObstacle[x][y] == 0:
                    return False
            else:
                if self.breezyMapReducedExpandObstacle[y][x] == 0:
                    return False
            f -= dy
            if f<0:
                y += ystep
                f += dx
        return True

    def heuristicCostEst(self, start, goal):
        return abs(start[1] - goal[1]) + abs(start[0] - goal[0])

    def nodeDistance(self, point1, point2):
        dx = abs(point1[0] - point2[0])
        dy = abs(point1[1] - point2[1])
        return math.sqrt(dx*dx + dy*dy)

    def pathPlanning(self):
        # A*
        closedset = {}
        openset = {}
        came_from = {}
        g_score = {}
        f_score = {}
        for key in self.samples:
            g_score[key] = sys.maxint 
            f_score[key] = sys.maxint
        g_score[self.startReduced] = 0
        f_score[self.startReduced] = g_score[self.startReduced] + self.heuristicCostEst(self.startReduced, self.endReduced)
        openset[self.startReduced] = f_score[self.startReduced] 
        #print "path planning with end", self.end
        while openset:
            current = min(openset, key=openset.get)
            if current == self.endReduced:
                return self.reconstructPath(came_from, self.endReduced)
            openset.pop(current)
            closedset[current] = []
            for neighbor in self.samples[current]:
                if neighbor in closedset:
                    continue
                tentativeGScore = g_score[current] + self.nodeDistance(current, neighbor)
                if tentativeGScore < g_score[neighbor]:
                    came_from[neighbor] = current
                    g_score[neighbor] = tentativeGScore
                    f_score[neighbor] = g_score[neighbor] + self.heuristicCostEst(neighbor, self.endReduced)
                    if neighbor not in openset:
                        openset[neighbor] = f_score[neighbor] 
        return False
    
    def reconstructPath(self, came_from, current):
        self.path = []
        self.path.append(current)
        while current in came_from:
            if current == self.lastStartReduced:
                break
            current = came_from[current]
            self.path.append(current)
        self.path.pop()

    def drawPos(self, isReduced=False):
        if isReduced:
            plt.plot(self.startReduced[1], self.startReduced[0], marker="o", color="b")
            plt.plot(self.endReduced[1], self.endReduced[0], marker="o", color="r")
        else:
            plt.plot(self.startReduced[1]*self.reduceRatio, self.startReduced[0]*self.reduceRatio, marker="o", color="b")
            plt.plot(self.endReduced[1]*self.reduceRatio, self.endReduced[0]*self.reduceRatio, marker="o", color="r")
    
    def drawSamples(self, isReduced=False):
        if isReduced:
            for key in self.samples:
                if key == self.startReduced or key == self.endReduced:
                    continue
                plt.plot(key[1], key[0], marker="x", color="b")
                neighbors = self.samples[key]
                for i in xrange(len(neighbors)):
                    plt.plot([key[1], neighbors[i][1]],[key[0],neighbors[i][0]],color="g")
        else:
            for key in self.samples:
                if key == self.startReduced or key == self.endReduced:
                    continue
                plt.plot(key[1]*self.reduceRatio, key[0]*self.reduceRatio, marker="x", color="b")
                neighbors = self.samples[key]
                for i in xrange(len(neighbors)):
                    plt.plot([key[1]*self.reduceRatio, neighbors[i][1]*self.reduceRatio],[key[0]*self.reduceRatio,neighbors[i][0]*self.reduceRatio],color="g")

    def drawPath(self, isReduced=False):
        if isReduced:
            for i in xrange(len(self.overallPath)-1):
                plt.plot([self.overallPath[i][1], self.overallPath[i+1][1]],[self.overallPath[i][0], self.overallPath[i+1][0]], color="r" )
        else:
            for i in xrange(len(self.overallPath)-1):
                plt.plot([self.overallPath[i][1]*self.reduceRatio, self.overallPath[i+1][1]*self.reduceRatio],[self.overallPath[i][0]*self.reduceRatio, self.overallPath[i+1][0]*self.reduceRatio], color="r" )

    def readMapFromFile(self, filename):
        self.updateBreezyMap(np.load(filename))

    def displayBreezyMap(self):
        if self.breezyMapReducedExpandObstacle is not None:
            #self.drawSamples(isReduced=True)
            #self.drawPos(isReduced=True)
            #self.drawPath(isReduced=True)
            self.drawPos()
            self.drawPath()
            #imgplot = plt.imshow(self.breezyMapReducedExpandObstacle)
            imgplot = plt.imshow(self.originalBreezyMap)
            plt.gray()
            plt.show()

    def getPath(self):
        path_abs = []
        for i in xrange(len(self.path)-1, -1, -1):
            path_abs.append((self.path[i][1]*self.reduceRatio/self.mm2pix, (self.mapSize_pix - self.path[i][0]*self.reduceRatio)/self.mm2pix))
        return path_abs

    def solvePath(self, start, breezyMap, isPix):
        self.updateStartPos(start, isPix)
        self.updateBreezyMap(breezyMap)
        # if len(self.path) > 0:
            # print "check can connect on", self.startReduced, self.path[-1]
        if len(self.path) == 0 or not self.checkCanConnect(self.startReduced, self.path[-1]):
           #if path is empty or the next point is not traversable, update path 
            self.randomSample()
            self.reduceSamples()
            self.connectSamples()
            self.pathPlanning()
            # print "recalc path"
            # print self.path
	if len(self.path) != 0:
            self.overallPath.append(self.path[-1])
        return self.getPath()

    def convertToReduced(self, pos):
        x = float2int(pos[0]*self.mm2pix) / self.reduceRatio
        y = (self.mapSize_pix - float2int(pos[1]*self.mm2pix)) / self.reduceRatio
        return (x, y)

def main():
    # r = RPMPlanner(startPos=(60, 200), endPos=(230, 120))
    # r = RPMPlanner(startPos=(60, 200), endPos=(146, 43))
    #pygame.init()
    #screen = pygame.display.set_mode((120, 120))
    r = PRMPlanner()
    r.updateEndPos((3000, 500), False)
    # r.updateEndPos((4780, 2450), False)
    m = np.load("test_map.npy")
    #imgplot = plt.imshow(m)
    #plt.gray()
    #plt.show()
    startUpdate = (3500, 3000)
    while 1:
        path = r.solvePath(startUpdate, m, False)
        if len(path) > 0:
            print path
            startUpdate = path[0]
            r.path.pop()
        if r.convertToReduced(startUpdate) == r.convertToReduced((3000, 500)):
            break
    #while 1:
        #screen.fill((255,255,255))
        #surf = pygame.surfarray.make_surface(r.breezyMapReducedExpandObstacle.astype('uint8'))
        #screen.blit(surf, (0, 0))
	#pygame.display.flip()
    r.displayBreezyMap()

if __name__ == "__main__":
    main()
