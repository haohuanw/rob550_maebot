#!/usr/bin/env python
# -*- coding: utf-8 -*-
import copy
import sys
import math
import numpy as np
import matplotlib.pyplot as plt
import heapq
import Queue

M_SQRT2 = 1.41421356237

class state(object):

    def __init__(self, x=-1, y=-1, k=(-1, -1)):
        self.x = x
        self.y = y
        self.k = k 
    
    def __eq__(self, s):
        return self.x == s.x and self.y == s.y

    def __ne__(self, s):
        return self.x != s.x or self.y != s.y

    def __gt__(self, s):
        if self.k[0]-0.00001 > s.k[0]:
            return True
        elif self.k[0] < s.k[0]-0.00001:
            return False
        return self.k[1] > s.k[1]

    # def lessequal(self, s):
        # if self.k[0] < s.k[0]:
            # return True
        # elif self.k[0] > s.k[0]:
            # return False
        # return self.k[1] < s.k[1] + 0.00001

    # def lessthan(self, s):
        # if self.k[0] + 0.000001 < s.k[0]:
            # return True
        # elif self.k[0] - 0.000001 > s.k[0]:
            # return False
        # return self.k[1] < s.k[1]

    def __cmp__(self, s):
        if self.k[0]-0.00001 > s.k[0]:
            return -1 
        elif self.k[0] < s.k[0]-0.00001:
            return 1
        if self.k[1] > s.k[1]:
            return -1
        else:
            return 1

    def __hash__(self):
        return self.x + 34245*self.y

    def __str__(self):
        return "x "+str(self.x)+" y "+str(self.y)+" k "+str(self.k)

class cellInfo():
    def __init__(self, g=0.0, rhs=0.0, cost=0.0):
        self.g = g
        self.rhs = rhs
        self.cost = cost

class DStar():
    
    def __init__(self, width, height):
        self.openList = Queue.PriorityQueue()
        self.cellHash = {}
        self.openHash = {}
        self.C1 = 1 
        self.path = []
        self.maxSteps = 80000
        self.boundary = (width, height)

    def isInBoundary(self, u):
        return u.x >= 0 and u.x < self.boundary[0] and u.y >= 0 and u.y <= self.boundary[1]

    def init(self, sx, sy, gx, gy):
        self.cellHash.clear()
        self.openHash.clear()
        self.path = []
        while not self.openList.empty():
            self.openList.get()
        self.k_m = 0
        self.s_start = state(x=sx, y=sy)
        self.s_goal = state(x=gx, y=gy)

        tmp = cellInfo()
        tmp.g = tmp.rhs = 0.0
        tmp.cost = self.C1
        self.cellHash[self.s_goal] = tmp

        tmp.g = tmp.rhs = self.heuristic(self.s_start, self.s_goal)
        tmp.cost = self.C1
        self.cellHash[self.s_start] = tmp
        self.s_start = self.calculateKey(self.s_start)

        self.s_last = self.s_start

    def updateCell(self, x, y, val):
        u = state()
        u.x = x
        u.y = y
        if u == self.s_start or u == self.s_goal:
            return
        self.makeNewCell(u)
        self.cellHash[u].cost = val

        self.updateVertex(u)

    def updateStart(self, x, y):
        self.s_start.x = x
        self.s_start.y = y

        self.k_m += self.heuristic(self.s_last, self.s_start)
        self.s_last = self.s_start

    def replan(self):
        self.path = []
        res = self.computeShortestPath()
        if res < 0:
            print "res<0 No path found"
            return False
        n = []

        cur = self.s_start
        
        if self.getG(self.s_start) == float("inf"):
            print "g == inf No path found"
            return False

        while cur != self.s_goal:
            print "cur: ", str(cur)
            self.path.append(cur)
            n.extend(self.getSucc(cur))
            if len(n) == 0:
                print "no sucessor No path found"
                return False
            for n_ in n:
                print str(n_)
            cmin = float("inf")
            tmin = 0.0
            smin = state()
            for n_ in n:
                val = self.cost(cur, n_)
                val2 = self.trueDist(n_, self.s_goal) + self.trueDist(self.s_start, n_)
                val += self.getG(n_)

                if self.close(val, cmin):
                    if tmin > val2:
                        tmin = val2
                        cmin = val
                        smin = n_
                elif val < cmin:
                    tmin = val2
                    cmin = val
                    smin = n_
            n = []
            cur = smin
        self.path.append(self.s_goal)
        for p in self.path:
            print str(p)
        return True

    def close(self, x, y):
        if x == float("inf") and y == float("inf"):
            return True
        return (abs(x-y) < 0.00001)

    def makeNewCell(self, u):
        if u in self.cellHash:
            return
        else:
            tmp = cellInfo()
            tmp.g = tmp.rhs = self.heuristic(u, self.s_goal)
            tmp.cost = self.C1
            self.cellHash[u] = tmp

    def getG(self, u):
        if not u in self.cellHash:
            return self.heuristic(u, self.s_goal)
        return self.cellHash[u].g

    def setG(self, u, g):
        self.makeNewCell(u)
        self.cellHash[u].g = g

    def getRHS(self, u):
        if u == self.s_goal:
            return 0
        if not u in self.cellHash:
            return self.heuristic(u, self.s_goal)
        return self.cellHash[u].rhs

    def setRHS(self, u, rhs):
        self.makeNewCell(u)
        self.cellHash[u].rhs = rhs

    def eightCondist(self, a, b):
        min_val = abs(a.x - b.x)
        max_val = abs(a.y - b.y)
        if min_val > max_val:
            tmp = min_val
            min_val = max_val
            max_val = tmp
        return (M_SQRT2-1.0) * min_val + max_val 
            
    def computeShortestPath(self):
        s = []
        if self.openList.empty():
            return 1
        k = 0
        while 1:
            if not self.openList.empty():
                break
            top = self.openList.get()
            self.openList.put(top)
            self.s_start = self.calculateKey(self.s_start)
            if self.s_start > top or self.getRHS(self.s_start) != self.getG(self.s_start):
                break
            if k > self.maxSteps:
                return -1
            k+=1

            u = state()

            test = (self.getRHS(self.s_start) != self.getG(self.s_start))

            while 1:
                if self.openList.empty():
                    return 1
                u = self.openList.get()

                if not self.isValid(u):
                    continue

                if (not (self.s_start > u)) and (not test):
                    return 2
                break
            self.openHash.pop(u) 

            k_old = u

            if self.calculateKey(u) > k_old:
                self.insert(u)
            elif self.getG(u) > self.getRHS(u):
                self.setG(u, self.getRHS(u))
                s.extend(self.getPred(u))
                for s_ in s:
                    self.updateVertex(s_)
            else:
                self.setG(u, float("inf"))
                s.extend(self.getPred(u))
                for s_ in s:
                    self.updateVertex(s_)
                self.updateVertex(u)
        return 0

    def updateVertex(self, u):
        s = []
        if u != self.s_goal:
            s.extend(self.getSucc(u))
            tmp = float("inf")
            for s_ in s:
                tmp2 = self.getG(s_) + self.cost(u, s_)
                if tmp2 < tmp:
                    tmp = tmp2
            if not self.close(self.getRHS(u), tmp):
                self.setRHS(u, tmp)
        if not self.close(self.getG(u), self.getRHS(u)):
            self.insert(u)

    def insert(self, u):
        u = self.calculateKey(u)
        csum = self.keyHashCode(u)
        self.openHash[u] = csum
        self.openList.put(u)

    def remove(self, u):
        if not u in self.cellHash:
            return
        self.openHash.pop(u)

    def trueDist(self, a, b):
        dx = a.x - b.x
        dy = a.y - b.y
        return math.sqrt(dx*dx + dy*dy)

    def heuristic(self, a, b):
        return self.eightCondist(a, b)*self.C1

    def calculateKey(self, u):
        val = min(self.getRHS(u), self.getG(u))
        u.k = (val + self.heuristic(u, self.s_start) + self.k_m, val)
        return u

    def getSucc(self, u):
        s = []
        if self.occupied(u):
            return s
        for i in xrange(-1,2):
            for j in xrange(-1,2):
                if i == 0 and j == 0:
                    continue
                newu = state(x=u.x+i, y=u.y+j, k=(-1, -1))
                s.append(newu)
        return s

    def getPred(self, u):
        s = []
        for i in xrange(-1,2):
            for j in xrange(-1,2):
                if i==0 and j ==0:
                    continue
                newu = state(x=u.x+i, y=u.y+j, k=(-1, -1))
                if not self.occupied(newu):
                    s.append(newu)
        return s

    def cost(self, a, b):
        dx = abs(a.x - b.x)
        dy = abs(a.y - b.y)
        scale = 1
        if (dx+dy) > 1:
            scale = M_SQRT2
        if not a in self.cellHash:
            return scale*self.C1
        return scale*self.cellHash[a].cost

    def occupied(self, u):
        if not self.isInBoundary(u):
            return True
        if not u in self.cellHash:
            return False
        return self.cellHash[u].cost < 0

    def isValid(self, u):
        if not u in self.openHash:
            return False
        if not self.close(self.keyHashCode(u), self.openHash[u]):
            return False
        return True

    def keyHashCode(self, u):
        return float(u.k[0] + 1193*u.k[1])

if __name__ == "__main__":
    # d = DStar()
    # d.readMapFromFile("test_map.npy")
    # d.displayBreezyMap()
    s = state(0,0,(2,2))
    s1 = state(0,0,(4,2))
    print s1 != s
    q = Queue.PriorityQueue() 
    q.put(state(0,0,(2,2)))
    q.put(state(0,0,(3,2)))
    q.put(state(0,0,(4,2)))
    q.put(state(0,0,(1,2)))
    while not q.empty():
        print q.get()
