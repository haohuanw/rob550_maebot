#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import pprint
import pygame
import numpy as np
from PIL import Image
from pygame.locals import *
from DStar import DStar
class Test():
    def __init__(self):
        self.breezyMap = np.array([[255 for _ in xrange(200)] for __ in xrange(200)])
        pygame.init()
        self.height = 200
        self.width = 200
        self.screen = pygame.display.set_mode((self.width, self.height))
        self.dstar = DStar(2, 2)

    def MainLoop(self):
        self.dstar.init(0, 0, 1, 1)
        while 1:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    sys.exit()
                elif event.type == KEYDOWN:
                    if event.key == K_q:
                        sys.exit()
                elif event.type == MOUSEBUTTONDOWN:
                    print len(self.breezyMap), len(self.breezyMap[0])
                    y, x = pygame.mouse.get_pos()
                    self.breezyMap[y][x] = 0
                    self.dstar.updateCell(y, x, -1)
            self.dstar.replan()
            print "replanned"
            self.screen.fill((255,255,255))
            surf = pygame.surfarray.make_surface(self.breezyMap.astype('uint8'))
            self.screen.blit(surf, (0, 0))
            pygame.display.flip()

if __name__ == "__main__":
    t = Test()
    t.MainLoop()
