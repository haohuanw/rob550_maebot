#!/usr/bin/env python
# -*- coding: utf-8 -*-
import copy
import sys
import math
import numpy as np
import matplotlib.pyplot as plt
from scipy import misc 

float2int = lambda x: int(0.5+x)
DEG_2_RAD = np.pi/180.0
#This version of code only do path planning around the start position
class PRMNav():

    def __init__(self, breezyMap=None, pathPlanningSize=40, sampleSize=20, edgeOffset=2, reduceRatio=3, neighborDist=5, MAP_SIZE_M=6.0, MAP_RES_PIX_PER_M=100, **unused):
        if breezyMap is not None:
            self.updateBreezyMap(breezyMap)
        else:
            self.breezyMap = breezyMap #[y][x], [row][col] for indexing
        self.samples = {} 
        self.sampleSize = sampleSize
        self.edgeOffset = edgeOffset 
        self.reduceRatio = reduceRatio
        self.neighborDist = neighborDist
        self.pathPlanningSize = pathPlanningSize/2
        self.path = []
        self.mm2pix = MAP_RES_PIX_PER_M/1000.0 # [pix/mm]
        self.mapSize_pix = int(MAP_SIZE_M*MAP_RES_PIX_PER_M)
        self.currentEnd = (-1, -1)

    def updateStartPos(self, startPos, isPix=True):
        if isPix:
            self.start = (startPos[1], startPos[0])
        else:
            xpix = float2int(startPos[0]*self.mm2pix)
            ypix = self.mapSize_pix-float2int(startPos[1]*self.mm2pix)
            self.start = (ypix, xpix)
        self.startReduced = (self.start[0]/self.reduceRatio, self.start[1]/self.reduceRatio) 
        # print self.startReduced
        upper_y = self.start[0] - self.pathPlanningSize
        upper_x = self.start[1] - self.pathPlanningSize
        lower_y = self.start[0] + self.pathPlanningSize + 1
        lower_x = self.start[1] + self.pathPlanningSize + 1
        if  upper_y < 0:
            upper_y = 0
        if  upper_x < 0:
            upper_x = 0
        if  lower_y >= self.mapSize_pix:
            lower_y = self.mapSize_pix - 1
        if  lower_y >= self.mapSize_pix:
            lower_x = self.mapSize_pix - 1

        self.pathPlanningArea = (upper_y, upper_x, lower_y, lower_x)
        self.startSmall = (self.start[0] - upper_y, self.start[1] - upper_x)
        self.startSmallReduced = (self.startSmall[0]/self.reduceRatio, self.startSmall[1]/self.reduceRatio)
        #print self.startReduced

    def updateEndPos(self, endPos, isPix=True):
        if isPix:
            self.end = (endPos[1], endPos[0])
            self.endReduced = (endPos[1]/self.reduceRatio, endPos[0]/self.reduceRatio)
            #print self.endReduced
        else:
            xpix = float2int(endPos[0]*self.mm2pix)
            ypix = self.mapSize_pix-float2int(endPos[1]*self.mm2pix)
            self.end = (ypix, xpix)
            self.endReduced = (ypix/self.reduceRatio, xpix/self.reduceRatio)
    
    
    def updateBreezyMap(self, breezyMap):
        self.originalBreezyMap = breezyMap
        self.breezyMap = copy.deepcopy(breezyMap)
        self.breezyMap = breezyMap[self.pathPlanningArea[0]:self.pathPlanningArea[2], self.pathPlanningArea[1]:self.pathPlanningArea[3]]
        row = len(self.breezyMap)
        col = len(self.breezyMap)
        for i in xrange(row):
            for j in xrange(col):
                if self.breezyMap[i][j] < 235:
                    self.breezyMap[i][j] = 0
                else:
                    self.breezyMap[i][j] = 255
        #only have a portion
        self.breezyMapReduced = self.breezyMap[::self.reduceRatio,::self.reduceRatio]
        row = len(self.breezyMapReduced)
        col = len(self.breezyMapReduced[0])
        self.breezyMapReducedExpandObstacle = [[255 for i in xrange(col)] for j in xrange(row)]
        for i in xrange(self.edgeOffset, row-self.edgeOffset):
            for j in xrange(self.edgeOffset, col-self.edgeOffset):
                if self.breezyMapReduced[i][j] == 0:
                    for m in xrange(self.edgeOffset):
                        for n in xrange(self.edgeOffset): 
                            self.breezyMapReducedExpandObstacle[i+m][j+n] = 0
                            self.breezyMapReducedExpandObstacle[i+m][j+n] = 0
                            self.breezyMapReducedExpandObstacle[i-m][j-n] = 0
                            self.breezyMapReducedExpandObstacle[i-m][j-n] = 0
                            self.breezyMapReducedExpandObstacle[i-m][j] = 0
                            self.breezyMapReducedExpandObstacle[i+m][j] = 0
                            self.breezyMapReducedExpandObstacle[i][j+n] = 0
                            self.breezyMapReducedExpandObstacle[i][j+n] = 0

    # now when we do random sample, it only give sample that is from 0~len(breezyMap)
    def randomSample(self):
        row = len(self.breezyMapReduced)
        col = len(self.breezyMapReduced[0])
        # assume row == col
        sampleRange = min(row, col)
        self.samples.clear()
        s = np.random.random_integers(sampleRange-1, size=(self.sampleSize, 2)) 
        for i in xrange(len(s)):
            self.samples[(s[i][0], s[i][1])] = []
        # print len(self.samples)

    def reduceSamples(self):
        for key,v in self.samples.items():
            if self.breezyMapReducedExpandObstacle[key[0]][key[1]] == 0:
                self.samples.pop(key)
        self.sampleSize = len(self.samples)

    def connectSamples(self):
        self.samples[self.startSmallReduced] = []
        self.samples[self.currentEndSmallReduced] = []
        for key in self.samples:
            for i in xrange(self.neighborDist):
                for j in xrange(self.neighborDist):
                    dest = []
                    dest.append((key[0]-i, key[1]-j))
                    dest.append((key[0]+i, key[1]+j))
                    dest.append((key[0]+i, key[1]-j))
                    dest.append((key[0]-i, key[1]+j))
                    dest.append((key[0]-i, key[1]))
                    dest.append((key[0]+i, key[1]))
                    dest.append((key[0], key[1]+j))
                    dest.append((key[0], key[1]-j))
                    for k in xrange(len(dest)):
                        if dest[k] in self.samples:
                            if self.checkCanConnect(key, dest[k]):
                                self.samples[key].append(dest[k])
            #print "key: ", (key[0], key[1]), "neighbors: ", self.samples[key]

    def updateCurrentEndPos(self):
        currentUpdate = (-1, -1)
        currentShortest = sys.maxint
        for key in self.samples:
            if self.checkCanConnect(self.startSmallReduced, key):
                y = key[0]*self.reduceRatio + self.pathPlanningArea[0]
                x = key[1]*self.reduceRatio + self.pathPlanningArea[1]
                dy = y - self.end[0]
                dx = x - self.end[1]
                distToEnd = math.sqrt(dy*dy+dx*dx)
                if distToEnd < currentShortest:
                    currentShortest = distToEnd
                    currentUpdate = (y, x)

        # for i in xrange(self.pathPlanningArea[0], self.pathPlanningArea[2]+1):
            # for j in xrange(self.pathPlanningArea[1], self.pathPlanningArea[3]+1):
                # dy = i-self.endReduced[0]
                # dx = j-self.endReduced[1]
                # distToEnd = math.sqrt(dy*dy+dx*dx)
                # if distToEnd < currentShortest:
                    # currentShortest = distToEnd
                    # currentUpdate = (i, j)
        self.currentEnd = (currentUpdate[0], currentUpdate[1])
        self.currentEndReduced = (currentUpdate[0]/self.reduceRatio, currentUpdate[1]/self.reduceRatio) # (y,x)
        self.currentEndSmall = (self.currentEnd[0] - self.pathPlanningArea[0], self.currentEnd[1] - self.pathPlanningArea[1])
        self.currentEndSmallReduced = (self.currentEndSmall[0]/self.reduceRatio, self.currentEndSmall[1]/self.reduceRatio)

    def getCurrentEnd(self):
        return (self.currentEnd[1], self.currentEnd[0])

    def checkCanConnect(self, point1, point2):
        if self.breezyMapReducedExpandObstacle[point1[0]][point1[1]] == 0:
            return False
        if self.breezyMapReducedExpandObstacle[point2[0]][point2[1]] == 0:
            return False
        x1 = point1[1]
        x2 = point2[1]
        y1 = point1[0]
        y2 = point2[0]
        steep = abs(y2-y1) > abs(x2-x1)
        if steep:
            x1, y1 = y1, x1
            x2, y2 = y2, x2
        if x1 > x2:
            x1, x2 = x2, x1
            y1, y2 = y2, y1
        dx = x2 - x1
        dy = abs(y2 - y1)
        f = dx/2
        ystep = 1 if y1<y2 else -1
        y = y1
        for x in xrange(x1, x2+1):
            if steep:
                if self.breezyMapReducedExpandObstacle[x][y] == 0:
                    return False
            else:
                if self.breezyMapReducedExpandObstacle[y][x] == 0:
                    return False
            f -= dy
            if f<0:
                y += ystep
                f += dx
        return True

    def heuristicCostEst(self, start, goal):
        return abs(start[1] - goal[1]) + abs(start[0] - goal[0])

    def nodeDistance(self, point1, point2):
        dx = abs(point1[0] - point2[0])
        dy = abs(point1[1] - point2[1])
        return math.sqrt(dx*dx + dy*dy)

    def pathPlanning(self):
        # A*
        closedset = {}
        openset = {}
        came_from = {}
        g_score = {}
        f_score = {}
        for key in self.samples:
            g_score[key] = sys.maxint 
            f_score[key] = sys.maxint
        g_score[self.startSmallReduced] = 0
        f_score[self.startSmallReduced] = g_score[self.startSmallReduced] + self.heuristicCostEst(self.startSmallReduced, self.currentEndSmallReduced)
        openset[self.startSmallReduced] = f_score[self.startSmallReduced] 

        while openset:
            current = min(openset, key=openset.get)
            if current == self.currentEndSmallReduced:
                return self.reconstructPath(came_from, self.currentEndSmallReduced)
            openset.pop(current)
            closedset[current] = []
            for neighbor in self.samples[current]:
                if neighbor in closedset:
                    continue
                tentativeGScore = g_score[current] + self.nodeDistance(current, neighbor)
                if tentativeGScore < g_score[neighbor]:
                    came_from[neighbor] = current
                    g_score[neighbor] = tentativeGScore
                    f_score[neighbor] = g_score[neighbor] + self.heuristicCostEst(neighbor, self.currentEndSmallReduced)
                    if neighbor not in openset:
                        openset[neighbor] = f_score[neighbor] 
        return False
    
    def reconstructPath(self, came_from, current):
        #path is in small reduced coord
        self.path = []         
        self.path.append(current)
        while current in came_from:
            current = came_from[current]
            self.path.append(current)
        #print self.path

    #draw in the original map 
    def drawPos(self):
        temp_endy = self.currentEndSmallReduced[0]*self.reduceRatio+self.pathPlanningArea[0]
        temp_endx = self.currentEndSmallReduced[1]*self.reduceRatio+self.pathPlanningArea[1]
        plt.plot(self.startReduced[1]*self.reduceRatio, self.startReduced[0]*self.reduceRatio, marker="o", color="b")
        plt.plot(self.endReduced[1]*self.reduceRatio, self.endReduced[0]*self.reduceRatio, marker="x", color="r")
        plt.plot(temp_endx, temp_endy, marker="o", color="r")
   
    #draw in the original map
    def drawSamples(self, isReduced=False):
        for key in self.samples:
            if key == self.startReduced or key == self.endReduced:
                continue
            #point
            y = key[0]*self.reduceRatio + self.pathPlanningArea[0]
            x = key[1]*self.reduceRatio + self.pathPlanningArea[1]
            plt.plot(x, y, marker="x", color="b")
            neighbors = self.samples[key]
            #lines
            for i in xrange(len(neighbors)):
                y1 = neighbors[i][0]*self.reduceRatio + self.pathPlanningArea[0]
                x1 = neighbors[i][1]*self.reduceRatio + self.pathPlanningArea[1]
                plt.plot([x,x1],[y,y1],color="g")

    def drawPath(self):
        for i in xrange(len(self.path)-1):
            y1 = self.path[i][0]*self.reduceRatio + self.pathPlanningArea[0]
            x1 = self.path[i][1]*self.reduceRatio + self.pathPlanningArea[1]
            y2 = self.path[i+1][0]*self.reduceRatio + self.pathPlanningArea[0]
            x2 = self.path[i+1][1]*self.reduceRatio + self.pathPlanningArea[1]
            plt.plot([x1, x2],[y1, y2], color="r" )

    def readMapFromFile(self, filename):
        self.updateBreezyMap(np.load(filename))

    def displayBreezyMap(self):
        if self.breezyMap is not None:
            self.drawSamples()
            self.drawPos()
            self.drawPath()
            imgplot = plt.imshow(self.originalBreezyMap)
            plt.gray()
            plt.show()

    def getPath(self):
        path_abs = []
        for i in xrange(len(self.path)-1, -1, -1):
            #convert back to full breezy map coord
            y = self.path[i][0]*self.reduceRatio + self.pathPlanningArea[0]
            x = self.path[i][1]*self.reduceRatio + self.pathPlanningArea[1]
            #covert to absolute
            path_abs.append((x/self.mm2pix, y/self.mm2pix))
        return path_abs

    def solvePath(self, start, breezyMap, isPix):
        self.updateStartPos(start, isPix)
        self.updateBreezyMap(breezyMap)
        self.randomSample()
        self.reduceSamples()
        self.updateCurrentEndPos()
        self.connectSamples()
        self.pathPlanning()
        return self.getPath()

def main():
    bmap = np.load("test_map.npy")
    r = PRMNav()
    r.updateEndPos((1980,2880), False)
    print r.solvePath((245, 302),bmap,True)
    #r.displayBreezyMap()

if __name__ == "__main__":
    main()
