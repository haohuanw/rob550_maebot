#!/usr/bin/env python
# -*- coding: utf-8 -*-

import copy
import sys
import math
import numpy as np
import matplotlib.pyplot as plt
import Queue

class PathPlanner():

    def __init__(self, breezyMap=None, edgeOffset=2, reduceRatio=3, MAP_SIZE_M=6.0, MAP_RES_PIX_PER_M=100, **unused):
        if breezyMap is not None:
            self.updateBreezyMap(breezyMap)
        else:
            self.breezyMap = breezyMap #[y][x], [row][col] for indexing
        self.edgeOffset = edgeOffset 
        self.reduceRatio = reduceRatio
        self.path = []
        self.mm2pix = MAP_RES_PIX_PER_M/1000.0 # [pix/mm]
        self.mapSize_pix = int(MAP_SIZE_M*MAP_RES_PIX_PER_M)

    def updateStartPos(self, startPos, isPix=True):
        if isPix:
            self.start = (startPos[1], startPos[0])
            self.startReduced = (startPos[1]/self.reduceRatio, startPos[0]/self.reduceRatio)
        else:
            xpix = float2int(startPos[0]*self.mm2pix)
            ypix = self.mapSize_pix-float2int(startPos[1]*self.mm2pix)
            self.start = (ypix, xpix)
            self.startReduced = (ypix/self.reduceRatio, xpix/self.reduceRatio)
        #print self.startReduced

    def updateEndPos(self, endPos, isPix=True):
        if isPix:
            self.end = (endPos[1], endPos[0])
            self.endReduced = (endPos[1]/self.reduceRatio, endPos[0]/self.reduceRatio)
            #print self.endReduced
        else:
            xpix = float2int(endPos[0]*self.mm2pix)
            ypix = self.mapSize_pix-float2int(endPos[1]*self.mm2pix)
            self.start = (ypix, xpix)
            self.endReduced = (ypix/self.reduceRatio, xpix/self.reduceRatio)

    def updateBreezyMap(self, breezyMap):
        self.originalBreezyMap = breezyMap
        self.breezyMap = copy.deepcopy(breezyMap)
        row = len(self.breezyMap)
        col = len(self.breezyMap[0])
        for i in xrange(row):
            for j in xrange(col):
                if self.breezyMap[i][j] < 235:
                    self.breezyMap[i][j] = 0
                else:
                    self.breezyMap[i][j] = 255
        self.breezyMapReduced = self.breezyMap[::self.reduceRatio,::self.reduceRatio]
        row = len(self.breezyMapReduced)
        col = len(self.breezyMapReduced[0])
        self.breezyMapReducedExpandObstacle = [[255 for i in xrange(col)] for j in xrange(row)]
        for i in xrange(self.edgeOffset, row-self.edgeOffset):
            for j in xrange(self.edgeOffset, col-self.edgeOffset):
                if self.breezyMapReduced[i][j] == 0:
                    for m in xrange(self.edgeOffset):
                        for n in xrange(self.edgeOffset): 
                            self.breezyMapReducedExpandObstacle[i+m][j+n] = 0
                            self.breezyMapReducedExpandObstacle[i-m][j-n] = 0
    def pathPlanning(slef):
        closedset = {}
        openset = {}
        came_from = {}
        g_score = {}
        f_score = {}

    def drawPos(self, isReduced=False):
        if isReduced:
            plt.plot(self.startReduced[1], self.startReduced[0], marker="o", color="b")
            plt.plot(self.endReduced[1], self.endReduced[0], marker="o", color="r")
        else:
            plt.plot(self.startReduced[1]*self.reduceRatio, self.startReduced[0]*self.reduceRatio, marker="o", color="b")
            plt.plot(self.endReduced[1]*self.reduceRatio, self.endReduced[0]*self.reduceRatio, marker="o", color="r")

    def drawPath(self):
        for i in xrange(len(self.path)-1):
            plt.plot([self.path[i][1]*self.reduceRatio, self.path[i+1][1]*self.reduceRatio],[self.path[i][0]*self.reduceRatio, self.path[i+1][0]*self.reduceRatio], color="r" )

    def readMapFromFile(self, filename):
        self.updateBreezyMap(np.load(filename))

    def displayBreezyMap(self):
        if self.breezyMap is not None:
            #self.drawPos()
            #self.drawPath()
            imgplot = plt.imshow(self.breezyMapReducedExpandObstacle)
            plt.gray()
            plt.show()

    def getPath(self):
        path_abs = []
        for i in xrange(len(self.path)-1, -1, -1):
            path_abs.append((self.path[i][1]*self.reduceRatio/self.mm2pix, (self.mapSize_pix - self.path[i][0]*self.reduceRatio)/self.mm2pix))
        return path_abs

