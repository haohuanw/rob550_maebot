#!/usr/bin/env python
# -*- coding: utf-8 -*-

#PID.py
#PID controller
import lcm

from lcmtypes import odo_pose_xyt_t
from lcmtypes import odo_dxdtheta_t
from lcmtypes import pid_init_t
from lcmtypes import velocity_cmd_t

class Maebot_Controller(object):

    def __init__(self):
        self.fowardVelocityPID = PIDController()
        # TODO:
        # Anguler Velocity or Angular Position?
        # From velocity_cmd_t we could get forward vel and angular vel
        self.angularPositionPID = PIDController()

        self.odoVxy = 0.0
        #self.odoVtheta = 0.0
        self.odoPosetheta = 0.0

        self.cmdVxy = 0.0
        #self.cmdVtheta = 0.0
        self.cmdPosetheta = 0.0 
        self.cmdPrevTime = 0.0
        self.cmdState = 0

        self.lc = lcm.LCM()
        lcmOdoSub = self.lc.subscribe("BOT_ODO_VEL", odoVelHandler)
        lcmPoseSub = self.lc.subscribe("BOT_ODO_POSE", odoPoseHandler)
        lcmPIDSub = self.lc.subscribe("GS_PID_INIT",  pidHandler)
        lcmVelCmdSub = self.lc.subscribe("GS_VELOCITY_CMD", velCmdHandler)

    def odoVelHandler(self, channel, data):
        msg = odo_dxdtheta_t.decode(data)
        self.odoVxy = msg.dxy / msg.dt
        # self.odoVtheta = msg.dtheta / msg.dt

    def odoPoseHandler(self, channel, data):
        msg = odo_pose_xyt_t.decode(data)
        self.odoPosetheta = msg.xyt[2]

    def pidHandler(self, channel, data):
        msg = pid_init_t.decode(data)
        self.fowardVelocityPID.setPIDparameter(msg.kp, msg.ki, msg.kd)
        self.angularPositionPID.setPIDparameter(msg.kp, msg.ki, msg.kd)

    def velCmdHandler(self, channel, data):
        msg = velocity_cmd_t.decode(data)
        if self.cmdState == 0:
            self.cmdPrevTime = msg.utime
            self.cmdVxy = msg.fwd_vel
            self.cmdVtheta = msg.ang_vel
            self.cmdPosetheta = 0.0
            self.cmdState = 1
        else:
            currTime = msg.utime
            timeElapsed = currTime - self.cmdPrevTime
            self.cmdPrevTime = currTime
            self.cmdVxy = msg.fwd_vel
            self.cmdVtheta = msg.ang_vel
            self.cmdPosetheta += self.cmdVtheta * timeElapsed * 0.000001

class PIDController(object):

    def __init__(self, p=0.0, i=0.0, d=0.0, updateRate=100000):
        self.kp = p 
        self.ki = i
        self.kd = d
        self.et = 0.0
        self.et1 = 0.0
        self.et2 = 0.0
        self.output = 0.0
        self.outputMax = 0.0
        self.outputMin = 0.0
        self.updateRate = updateRate 

    def getOutput(self):
        return self.output
       
    def computeOutput(self):
        self.output += self.kp*(self.et - self.et1) \
                    + self.ki*self.et \
                    + self.kd*(self.et - 2*self.et1 + self.et2)
        if self.output > self.outputMax:
            self.output = self.outputMax
        elif self.output < self.outputMin:
            self.output = self.outputMin

    def updateError(self, errorInput):
        self.et2 = self.et1
        self.et1 = self.et
        self.et = errorInput

    def setPIDparameter(self, p, i, d):
        self.kp = p
        self.ki = i
        self.kd = d

    def setOutputLimit(self, maxVal, minVal):
        self.outputMax = maxVal
        self.outputMin = minVal

    # def setUpdateRate(self, updateRate):
        # self.updateRate = updateRate
